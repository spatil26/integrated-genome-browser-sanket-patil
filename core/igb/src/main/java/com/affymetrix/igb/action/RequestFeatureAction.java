/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.affymetrix.igb.action;

import com.affymetrix.genometry.event.GenericAction;
import com.affymetrix.genometry.event.GenericActionHolder;
import com.affymetrix.genometry.util.GeneralUtils;
import static com.affymetrix.igb.IGBConstants.BUNDLE;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;

/**
 *
 * @author auser
 */
public class RequestFeatureAction extends GenericAction {

    private static final long serialVersionUID = 1L;
    private static final RequestFeatureAction ACTION = new RequestFeatureAction();

    static {
        GenericActionHolder.getInstance().addGenericAction(ACTION);
    }

    public static RequestFeatureAction getAction() {
        return ACTION;
    }

    private RequestFeatureAction() {
        super(BUNDLE.getString("requestAFeature"), null,
                "16x16/actions/mail-forward.png",
                "22x22/actions/mail-forward.png",
                KeyEvent.VK_R, null, true);
        this.ordinal = 140;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        super.actionPerformed(e);
        GeneralUtils.browse("http://sourceforge.net/p/genoviz/feature-requests/");
    }
}
